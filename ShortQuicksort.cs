﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShortQuicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] unsorted = { "h", "e", "l", "l", "o", " ", "w", "o", "r", "l", "d" };

            foreach (var item in unsorted)
                Console.Write(item + " ");
            Console.WriteLine();

            foreach (var item in Quicksort(unsorted))
                Console.Write(item + " ");
            Console.WriteLine();

            Console.ReadLine();
        }


        public static IEnumerable<IComparable> Quicksort(IEnumerable<IComparable> i)
        {
            return i.Any() ?  Quicksort(i.Where(x => 0 > x.CompareTo(i.ElementAt(0)))).Concat(i.Where(x => x == i.ElementAt(0))).Concat(Quicksort(i.Where(x => 0 < x.CompareTo(i.ElementAt(0))))) :  i;
        }
    }
}

